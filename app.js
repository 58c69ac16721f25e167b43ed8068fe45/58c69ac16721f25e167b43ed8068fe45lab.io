const express = require("express");

const server = express();

server.use(express.static("public"));

server.listen(3000, function() {
    console.log("开发模式: http://localhost:3000");
})